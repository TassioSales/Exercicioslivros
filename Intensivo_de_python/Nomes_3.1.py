"""' Armazene os nomes de alguns de seus amigos em uma lista
chamada names. Exiba o nome de cada pessoa acessando cada elemento da
lista, um de cada vez."""

nomeAmigo = ['Pedro', 'Thiagol', 'Robson', 'Fillipe', 'Jhon presley', 'Ruan']

print(nomeAmigo[0], end=' ')
print(nomeAmigo[1], end=' ')
print(nomeAmigo[2], end=' ')
print(nomeAmigo[3], end=' ')
print(nomeAmigo[4], end=' ')
print(nomeAmigo[5], end=' ')

print('\n')

for itens in nomeAmigo:
    print(itens, end=' ')

