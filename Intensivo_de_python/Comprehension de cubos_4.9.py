"""4.9 – Comprehension de cubos: Use uma list comprehension para gerar uma lista
dos dez primeiros cubos.
"""

num_list = [num ** 3 for num in range(1, 11)]
print(num_list)
