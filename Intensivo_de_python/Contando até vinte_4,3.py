"""4.3 – Contando até vinte: Use um laço for para exibir os números de 1 a 20,
incluindo-os.
"""
for c in range(0, 20):
    print(c + 1, end=" ")
